package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"time"
)

//TODO: Verify acme.json exists
//TODO: Watch for changes

/*
************************************************************************************************************************
Generic Functions & Structs
************************************************************************************************************************
*/

// errorString is a trivial implementation of error.
type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

func checkPanic(e error) {
	if e != nil {
		panic(e)
	}
}

func checkSoft(e error) {
	if e != nil {
		// notice that we're using 1, so it will actually log the where
		// the error happened, 0 = this function, we don't want that.
		pc, _, line, _ := runtime.Caller(1)

		fmt.Printf("[%s:%d] %v\n", runtime.FuncForPC(pc).Name(), line, "Error: " + e.Error())
		return
	}
}

// New returns an error that formats as the given text.
func New(text string) error {
	return &errorString{text}
}

//this logs the function name as well.
func prettyPrintLine(string string) {
	// notice that we're using 1, so it will actually log the where
	// the error happened, 0 = this function, we don't want that.
	pc, _, line, _ := runtime.Caller(1)

	fmt.Printf("[%s:%d] %v\n", runtime.FuncForPC(pc).Name(), line, string)
	return
}

func prettyPrintJSON(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

func printDateStamp(string string) {
	fmt.Println("[" + time.Now().Format(time.RFC850) + "] " + string)
}



/*
************************************************************************************************************************
Variables
************************************************************************************************************************
*/
const ACME_FILE = "acme.json"
const OUTPUT_DIR = "certificates"


/*
************************************************************************************************************************
Structs
************************************************************************************************************************
*/
type acmeConfig struct {
	Account 			*acmeAccount					`json:"Account,omitempty"`
	Certificates 		*[]acmeCertificate				`json:"Certificates,omitempty"`
	HTTPChallenges		*acmeHTTPChallenges				`json:"HTTPChallenges,omitempty"`
}

type acmeAccount struct {
	Email				string							`json:"Email,omitempty"`
	Registration		*acmeAccountRegistration		`json:"Registration,omitempty"`
	PrivateKey 			string							`json:"PrivateKey,omitempty"`
}

type acmeAccountRegistration struct {
	Body				*acmeAccountRegistrationBody	`json:"body,omitempty"`
	Uri  				string							`json:"uri,omitempty"`
}

type acmeAccountRegistrationBody struct {
	Status 				string							`json:"status,omitempty"`
	Contact				[]string						`json:"contact,omitempty"`
}

type acmeCertificate struct {
	Domain				*acmeCertificateDomain			`json:"Domain,omitempty"`
	Certificate			string							`json:"Certificate,omitempty"`
	Key 				string							`json:"Key,omitempty"`
}

type acmeCertificateDomain struct {
	Main 				string 							`json:"Main,omitempty"`
	SANs				[]string						`json:"SANs"`
}

type acmeHTTPChallenges struct {

}

/*
************************************************************************************************************************
Main Program
************************************************************************************************************************
*/
func main() {
	var config acmeConfig
	readConfigJSON(&config)

	printDateStamp("**************************************************")
	printDateStamp("**************************************************")
	printDateStamp("Starting Conversion")
	printDateStamp("**************************************************")
	printDateStamp("**************************************************")

	for _, certificateData := range *config.Certificates {
		keyString, err := base64.StdEncoding.DecodeString(certificateData.Key)
		if err != nil {
			fmt.Println("error:", err)
			return
		}
		//fmt.Printf("%s - %q\n", certificateData.Domain.Main, keyString)

		certificateString, err := base64.StdEncoding.DecodeString(certificateData.Certificate)
		if err != nil {
			fmt.Println("error:", err)
			return
		}
		//fmt.Printf("%s - %q\n", certificateData.Domain.Main, certificateString)

		createDirectory(OUTPUT_DIR)

		printDateStamp("Domain: " + certificateData.Domain.Main)
		writeCertToFile(certificateData.Domain.Main, certificateString, keyString)

		for _, san := range certificateData.Domain.SANs {
			printDateStamp("SAN: " + san)
			writeCertToFile(san, certificateString, keyString)
		}
	}
	printDateStamp("**************************************************")
	printDateStamp("**************************************************")
	printDateStamp("Conversion Complete")
	printDateStamp("**************************************************")
	printDateStamp("**************************************************")
}

/*
************************************************************************************************************************
Helper Functions
************************************************************************************************************************
*/

func readConfigJSON(config *acmeConfig) {
	// Open our jsonFile
	jsonFile, err := os.Open(ACME_FILE)
	// if we os.Open returns an error then handle it
	if err != nil {
		prettyPrintLine(err.Error())
	}
	fmt.Println("Successfully Opened " + ACME_FILE)
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'config' which we defined above
	json.Unmarshal(byteValue, &config)

	//fmt.Println(prettyPrintJSON(config))
}

func writeCertToFile(domain string, certificate []byte, key []byte) {
	writeFile(OUTPUT_DIR + string(os.PathSeparator) + domain + ".pem", certificate)
	writeFile(OUTPUT_DIR + string(os.PathSeparator) + domain + ".key", key)
}

func createDirectory(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path, os.ModePerm)
	}
}

func writeFile(path string, content []byte) {
	f, err := os.Create(path)
	checkPanic(err)

	_, err = f.Write(content)
	checkPanic(err)

	f.Close()
}